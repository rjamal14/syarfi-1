import Vue from 'vue'
import Router from 'vue-router'

import { 
    BootstrapVue, 
    IconsPlugin,
    NavbarPlugin,
    ButtonPlugin,
    FormPlugin
} from 'bootstrap-vue'

import { VueReCaptcha } from 'vue-recaptcha-v3'

import VueSweetalert2 from 'vue-sweetalert2';

const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674',
};

import VueSession from 'vue-session'
Vue.use(VueSession)


// For more options see below

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../assets/css/style.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(NavbarPlugin)
Vue.use(ButtonPlugin)
Vue.use(FormPlugin)
Vue.use(VueReCaptcha, { siteKey: '6Lfcyd8UAAAAANTsdBw3oJb0gv5RXHXyjJaWiu8R' })
Vue.use(VueSweetalert2, options)

import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
Vue.use(datePicker);

Vue.component('Header', require('../components/Header.vue').default);
Vue.component('Header2', require('../components/Header2.vue').default);
Vue.component('Footer', require('../components/Footer.vue').default);

import Home from '@/pages/Home'
import About from '@/pages/About'
import Register from '@/pages/Register'
import Login from '@/pages/Login'
import Panel from '@/pages/Panel'


Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/About',
            name: 'About',
            component: About
        },
        {
            path: '/Register',
            name: 'Register',
            component: Register
        },
        {
            path: '/Login',
            name: 'Login',
            component: Login
        },
        {
            path: '/Panel',
            name: 'Panel',
            component: Panel
        }
    ]
})