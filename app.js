global.mysql = require('mysql');
global.express = require('express');
global.app = express();
global.bodyparser = require('body-parser');

app.use(bodyparser.json({
    limit: '50mb'
}));
app.use(bodyparser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));

var cors = require('cors');
app.use(cors({
    origin: [
        'http://localhost:8080'
    ],
    credentials: true
}));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Headers', true);
    res.header('Access-Control-Allow-Credentials', "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Origin");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

app.get('/callback', (req, res) => {
    consol.log(req.query);
});

const Cryptr = require('cryptr');
const cryptr = new Cryptr('R4bdU1J4m41');
global.encrypt = (obj) => {
    const encryptedString = cryptr.encrypt(obj);
    return encryptedString
}

global.decrypt = (obj) => {
    const decryptedString = cryptr.decrypt(obj);
    return decryptedString
}

class RestServer {
    constructor() {
        this.setupPath();
        app.use(bodyparser.json());
        app.use(express.static('public'));
        global.date = require('date-format');
        global.async = require('async');
        this.setupAuth();
        this.setupLog();
        this.setupdb();
        this.setupRouter();
        this.setupHadleError();
    }

    setupPath() {
        var path = require('path');
        global.appDir = path.dirname(require.main.filename);
    }

    setupAuth() {
        const basicAuth = require('express-basic-auth')
        app.use(basicAuth({
            users: {
                'Syarfi': 'MDRPANTARA91HOST'
            }
        }))
    }


    setupLog() {
        var dd = date("yy-MM-dd");
        global.SimpleNodeLogger = require('simple-node-logger');
        var opts = {
            logFilePath: 'log/log-' + dd + '.log',
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        };
        global.log = SimpleNodeLogger.createSimpleLogger(opts);
    }

    setupdb() {
        mysqlConnection = require(appDir + '/dbconfig');
    }

    setupRouter() {
        var models_router = require(appDir + '/router/models_router');
        var controllers = require(appDir + '/router/controllers_router');
        models_router();
        controllers();
    }

    setupHadleError() {
        app.use((req, res, next) => {
            const error = new Error('Not founds');
            error.status = 404;
            next(error);
        })

        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
            //  res.render('error', {
            //      message: err.message,
            //      error: err
            //  });
        });

        process.on('uncaughtException', function (err) {
            log.info('Caught exception: ', err);
        });

    }

    log_error(message, err, res) {
        log.error(message, JSON.stringify(err));
        res.status(500).json({
            status: 'error',
            message: message,
            result: err
        });
        res.end();
    }

}

global.RestServerApp = new RestServer();

module.exports = app;
