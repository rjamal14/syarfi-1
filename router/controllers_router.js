var controllers_router = function () {
    const User_register = require(appDir + '/controllers/User/Register');
    app.use('/User/Register', User_register);

    const User_login = require(appDir + '/controllers/User/Login');
    app.use('/User/Login', User_login);
}

module.exports = controllers_router;
