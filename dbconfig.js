var mysql = require('mysql');
global.mysqlConnection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '140419',
    database: 'syarfi',
});

mysqlConnection.getConnection(function (err, connection) {
    if (!err)
        console.log('===> DB connection succeded');
    else
        console.log('===> DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

module.exports = mysqlConnection;
