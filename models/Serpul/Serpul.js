var mysqlConnection = require(appDir + '/dbconfig');
var Serpul = {
    getAll: function (callback) {
        return mysqlConnection.query("Select * from ppob_prabayar where status = 1", callback);
    },
    getPrefix: function (query, callback) {
        return mysqlConnection.query("Select prefix.nama_prefix, prefix.image, provider.nama_provider from prefix, provider where prefix.id_provider=provider.id and prefix.prefix like ?", '%' + query + '%', callback);
    },
    getById: function (query, callback) {
        return mysqlConnection.query("Select prefix.nama_prefix, prefix.image, provider.nama_provider, ppob_prabayar.* from prefix, provider, ppob_prabayar where prefix.id_provider=provider.id and ppob_prabayar.id_provider=prefix.id_provider and ppob_prabayar.id=?", query, callback);
    },
    getByType: function (query, callback) {
        return mysqlConnection.query("Select provider.nama_provider, ppob_prabayar.* from provider, ppob_prabayar where  ppob_prabayar.id_provider=provider.id and ppob_prabayar.type=?", query, callback);
    }



};
module.exports = Serpul;
