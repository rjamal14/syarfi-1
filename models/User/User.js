var mysqlConnection = require(appDir + '/dbconfig');
var User = {
    getAll: function (callback) {
            return mysqlConnection.query("Select * from user_pengguna where pengguna_status = 1", callback);
        },
    getById: function (id, callback) {
            return mysqlConnection.query("Select * from user_pengguna where pengguna_id=?", [id], callback);
        },
    getByEmail: function (id, callback) {
        return mysqlConnection.query("Select * from user_pengguna where pengguna_email=?", [id], callback);
    },
    save: function (data, callback) {
            return mysqlConnection.query("Insert into user_pengguna SET ?", data, callback);
        },
    delete: function (id, callback) {
            return mysqlConnection.query("delete from user_pengguna where pengguna_id=?", [id], callback);
        },
    update: function (id, arr, callback) {
            return mysqlConnection.query("update user_pengguna set ? where pengguna_id=?", [arr, id], callback);
        }



};
module.exports = User;
