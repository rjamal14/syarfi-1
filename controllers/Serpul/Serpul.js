const router = express.Router();
let {
    msleep
} = require('usleep');
const {
    check,
    validationResult
} = require('express-validator/check');
var base64Img = require('base64-img');
var async = require('async');
const {
    eachOf
} = require('async');
app.use(express.json());

const axios = require('axios');
const queryString = require('query-string');

axios.defaults.baseURL = 'https://portalpulsa.com/api/connect/';
axios.defaults.headers.common['portal-userid'] = 'P132453';
axios.defaults.headers.common['portal-key'] = '3d0cb8882eb13ef65f61bdbf99cc824e';
axios.defaults.headers.common['portal-secret'] = '9ef412f04008b8aebcdb90605b4f76708fc6f42ce5649625df08943c2b796546';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

var reqBody = [
    check('code').isLength({
        min: 1
    }).withMessage('Not empty')
];

router.post('/', reqBody, (req, res, next) => {
    errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array()
        });
    } else {
        var provider;
        var icon;
        var code = req.body.code;
        var phone = req.body.phone;

        async.waterfall([
            function (callback) {
                if (phone) {
                    var n = phone;
                    var r = n.substring(0, 1);
                    if (r == '0') {
                        phone = n.replace('0', '62')
                        phone = phone.substring(0, 5)
                        callback(null);
                    } else {
                        phone = n.substring(0, 5);
                        callback(null);
                    }
                } else {
                    callback(null)
                }

            },
            function (callback) {
                if (phone) {
                    Serpul.getPrefix(phone, (err, rows) => {
                        if (!err && rows.length > 0) {
                            icon = rows[0].image;
                            provider = rows[0].nama_provider;
                            callback(null)
                        } else {
                            console.log(err)
                        }
                    })
                } else {
                    callback(null)
                }
            },
        ], (err) => {
            axios.post('/', queryString.stringify({
                inquiry: 'HARGA',
                code: code
            })).then(function (response) {
                var data_harga;
                var data_res = response.data.message
                if (provider) {
                    data_harga = data_res.filter((obj) => {
                        return obj.provider == provider;
                    })
                } else {
                    data_harga = data_res
                }

                res.status(200).json({
                    status: 'success',
                    result: data_harga
                });
            }).catch(function (error) {
                console.log(error);
            })
        })
    }
})

module.exports = router;
