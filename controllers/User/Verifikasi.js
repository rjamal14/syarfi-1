const router = express.Router();
const async = require('async');
let { msleep } = require('usleep');
const { eachOf } = require('async');
const queryString = require('query-string');
const base64Img = require('base64-img');
const {
    check,
    validationResult
} = require('express-validator/check');


app.use(express.json());



var reqBody = [
    check('name').isLength({ min: 1 }).withMessage('Not empty'),
    check('email').isLength({ min: 1 }).withMessage('Not empty'),
    check('phone').isLength({ min: 1 }).withMessage('Not empty'),
    check('password').isLength({ min: 1 }).withMessage('Not empty')
];

router.post('/Pengguna', reqBody, (req, res, next) => {
    errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array()
        });
    } else {

        async.waterfall([
            function (callback) {
                data = {
                    pengguna_name: req.body.name,
                    pengguna_email: req.body.email,
                    pengguna_phone: req.body.phone,
                    pengguna_password: encrypt(req.body.password)
                }
                User.save(data, (err, rows) => {
                    if (!err) {
                        callback(null)
                    }
                    else if (err.code == 'ER_DUP_ENTRY' && err.sqlMessage == "Duplicate entry '" + req.body.email + "' for key 'user_pengguna.pengguna_email_UNIQUE'")
                        res.status(200).json({
                            status: 'error',
                            message: 'Email sudah digunakan!'
                        })
                    else if (err.code == 'ER_DUP_ENTRY' && err.sqlMessage == "Duplicate entry '" + req.body.phone + "' for key 'user_pengguna.pengguna_phone_UNIQUE'")
                        res.status(200).json({
                            status: 'error',
                            message: 'Nomor sudah digunakan!'
                        })
                    console.log(err)
                })
            },
        ], (err) => {
            if (!err) {
                res.status(200).json({
                    status: 'success',
                    message: "Selemat Register berhasil"
                });
            }
        })
    }
})

module.exports = router;
