const router = express.Router();
const async = require('async');
let {
    msleep
} = require('usleep');
const {
    eachOf
} = require('async');
const queryString = require('query-string');
const base64Img = require('base64-img');
const {
    check,
    validationResult
} = require('express-validator/check');


app.use(express.json());



var reqBody = [
    check('email').isLength({
        min: 1
    }).withMessage('Not empty'),
    check('password').isLength({
        min: 1
    }).withMessage('Not empty')
];

router.post('/Pengguna', reqBody, (req, res, next) => {
    errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array()
        });
    } else {
        async.waterfall([
            function (callback) {
                User.getByEmail(req.body.email, (err, rows) => {
                    (!err && rows.length > 0) ? callback(null, rows[0]): callback("user tidak terdaftar");
                })
            },
            function (data, callback) {
                user_password = decrypt(data.pengguna_password)
                if (user_password == req.body.password) {
                    callback(null, data);
                } else {
                    callback("password salah " + user_password)
                }
            }
        ], (err, data) => {
            if (!err)
                res.status(200).json({
                    status: 'success',
                    message: 'Login berhasil',
                    result: data
                })
            else
                res.status(200).json({
                    status: 'error',
                    message: err
                })
        })
    }
})

module.exports = router;
